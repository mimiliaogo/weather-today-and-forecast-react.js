import React from 'react';
import PropTypes from 'prop-types';

import './WeatherDisplay.css';

export default class WeatherForecastDisplay extends React.Component {
    static propTypes = {
        masking: PropTypes.bool,
        group: PropTypes.string,
        description: PropTypes.string,
        temp: PropTypes.number,
        unit: PropTypes.string
    };

    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div className={`weather-display ${this.props.masking
                ? 'masking'
                : ''}`}>
                <img src={`images/w-${this.props.first.group}.png`}/>
                <p className='description'>{this.props.first.description}</p>&nbsp;
                <h1 className='temp'>
                    <span className='display-3'>{this.props.first.temp.toFixed(0)}&ordm;</span>
                    &nbsp;{(this.props.unit === 'metric')
                        ? 'C'
                        : 'F'}
                </h1>
            </div>
        );
    }
}
