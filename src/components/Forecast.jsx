import React from 'react';
import PropTypes from 'prop-types';
import WeatherForecastDisplay from 'components/WeatherForecastDisplay.jsx';
import WeatherTableDisplay from 'components/WeatherTableDisplay.jsx';
import WeatherForm from 'components/WeatherForm.jsx';
import { getForecast } from 'api/open-weather-map.js';

import './weather.css';

export default class Forecast extends React.Component {

    static getInitWeatherState() {
        return {
            city: 'na',
            first: {
                code: -1,
                group: 'na',
                description: 'N/A',
                temp: NaN
            },
            second: {
                code: -1,
                group: 'na',
                description: 'N/A',
                temp: NaN
            },
            third: {
                code: -1,
                group: 'na',
                description: 'N/A',
                temp: NaN
            },
            forth: {
                code: -1,
                group: 'na',
                description: 'N/A',
                temp: NaN
            },
            fifth: {
                code: -1,
                group: 'na',
                description: 'N/A',
                temp: NaN
            }

        }
    }
    constructor(props) {
        super(props);

        this.state = {
            ...Forecast.getInitWeatherState(),
            loading: false,
            masking: false
        };

        // TODO
        this.handleFormQuery = this.handleFormQuery.bind(this);
    }
    componentDidMount() {
        this.getForecast(this.props.city, this.props.unit);
    }

    componentWillUnmount() {
        if (this.state.loading) {
            cancelForecast();
        }
    }
    render() {
        return (
            <div className={`forecast weather-bg ${this.state.first.group}`}>
                <div className={`mask ${this.state.masking ? 'masking' : ''}`}>
                    <WeatherForecastDisplay {...this.state} />
                    <WeatherForm city={this.props.city} unit={this.props.unit} onQuery={this.handleFormQuery} />
                    <WeatherTableDisplay {...this.state}  />
                </div>
            </div>
        );
    }

    getForecast(city, unit) {
        this.setState(
            {
                loading: true,
                masking: true,
                city: city
            }, () => {
                getForecast(city, unit).then(weather => {
                    this.setState({
                        ...weather,
                        loading: false
                    }, () => {
                        this.notifyUnitChange(unit);
                        this.notifyCityChange(city);
                    });
                }).catch(err => {
                    console.error('Error getting weather', err);
                    this.setState({
                        ...Forecast.getInitWeatherState(unit),
                        loading: false
                    }, () => {
                        this.notifyUnitChange(unit);
                        this.notifyCityChange(city);
                    });
                });
            }
        );
        setTimeout(() => {
            this.setState({
                masking: false
            });
        }, 600);

    }
    handleFormQuery(city, unit) {
        this.getForecast(city, unit);
    }

    notifyUnitChange(unit) {
        if (this.props.units !== unit) {
            this.props.onUnitChange(unit);
        }
    }
    notifyCityChange(city) {
        if (this.props.city !== city) {
            this.props.onCityChange(city);
        }
    }
}
